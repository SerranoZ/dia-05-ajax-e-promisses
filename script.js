const botao = document.querySelector(".botao-enviar")
const botaoeditar = document.querySelector(".Enviar botao-msg")
const mensagens = document.querySelector(".mensagens")
const url = "https://treinamento-ajax-api.herokuapp.com/messages"
const messageInput = document.querySelector('.texto')
//let texto = document.querySelector(".texto")

botao.addEventListener("click", (event) => {
    const messageInput = document.querySelector('.texto')

    const body = { 
        message: {
            content: messageInput.value,
            author: "Pelé"
        }
    }

    const config = {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {"Content-Type": "application/json"}
    }

    fetch(url, config)
    .then(response => response.json())
    .then(message => {
        createPostDiv(message)
    })
    .catch((err) => {
        alert("Erro")
    });
})

/*botaoeditar.addEventListener("click", (event) => {
    const editInput = document.querySelector('input-editar')

    const body = { 
        message: {
            content: editInput.value
        }
    }

    const config = {
        method: 'PUT',
        body: JSON.stringify(body),
        headers: {"Content-Type": "application/json"}
    }

    fetch(url, config)
    .then(response => response.json())
    .then(() => {
        alert("Mensagem foi editada!")
        console.log(message)
    })
    .catch((err) => {
        alert("Erro")
    });
})*/


function createPostDiv (message) {
    const li = document.createElement("li")
    
    li.className = "mensagem"
    li.innerHTML = `
    <h1>${message.author}</h1>
    <p class="texto-msg">${message.content}</p>
    <div class="botoes">
        <input type="button" value="Editar" class="Editar botao-msg" onclick="editar(this)")>
        <input type="button" value="Deletar" class="Deletar botao-msg" onclick="deleteMessages(${message.id})")>
    </div>
    <div class="botoes-modo-editar" hidden>
        <input id="input-editar" class="input-editar">
        <input type="button" value="Enviar" class="Enviar botao-msg" onclick="updateMessages(${message.id})">
    </div>`

    mensagens.appendChild(li)
}









//function deletar(elemento){
//    const parent = elemento.parentElement.parentElement
//    parent.remove()
//}

function editar(elemento) {
    
    const parent = elemento.parentNode.parentNode
    parent.querySelector(".botoes").toggleAttribute("hidden")
    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")
}

function editarTexto(elemento) {
    const parent = elemento.parentNode.parentNode
    const textArea = elemento.parentNode.children[0]

    parent.querySelector(".texto-msg").innerText = textArea.value
 
    parent.querySelector(".botoes").toggleAttribute("hidden")

    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")

    textArea.value = ""
}


const getMessages = () => {
    fetch(url)
    .then(response => response.json())
    .then(messages => {
        messages.forEach(message => {
            createPostDiv(message)
        })
    })
    .catch((err) => {
        alert("Ocorreu um erro!")
    })
}


const deleteMessages = (id) => {
    fetch(url + "/" + id, {
        method: "DELETE"
    })
    .then(() => {
        alert("Mensagem foi apagada!")
        console.log(message)
    })
    .catch((error) => {
        console.log(error)
    })
}

/*botaoeditar.addEventListener("click", (event) => {
    const editInput = document.querySelector('input-editar')

    const body = { 
        message: {
            content: editInput.value
        }
    }

    const config = {
        method: 'PUT',
        body: JSON.stringify(body),
        headers: {"Content-Type": "application/json"}
    }

    fetch(url, config)
    .then(response => response.json())
    .then(() => {
        alert("Mensagem foi editada!")
        console.log(message)
    })
    .catch((err) => {
        alert("Erro")
    });
})*/


const updateMessages = (id) => {
    const editInput = document.getElementById("input-editar")
    const body = {
        message: {
            content: editInput.value
        }
    }
    
    fetch(url + "/" + id, {
        method: "PUT",
        body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then((response) => response.json())
    .then((messages) => {
        alert("Mensagem foi editada!")
        console.log(messages)
    })
    .catch((error) => {
        console.error(error)
    })
}

getMessages()


/*const postMessages = () => {
    
    const body = {
        message: {
            content: "mais um teste nessa noite de sexta feira",
            author: "Rafael Aguiar Martins"
        }
    }

    const config = {
        method: "POST",
        body: JSON.stringify(body),
        headers: { 
            "Content-Type": "application/json"
        }
    }

    fetch(url + "/messages", config)
    .then((response) => response.json())
    .then((messages) => {
        alert("Mensagem criada")
        console.log(messages)
    })
    .catch((error) => {
        console.error(error)
    })
}


postMessages()

$.querySelector("botao-enviar").addEventListener('click', postMessages)

*/